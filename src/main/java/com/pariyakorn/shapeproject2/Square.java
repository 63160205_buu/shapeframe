/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject2;

/**
 *
 * @author acer
 */
public class Square extends Shape {
    private double side;
    public Square(double side) {
        super("Square");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    
    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public double calPerimeter() {
        return side * 4;
    }
    
}
