/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class RectangleFrame extends JFrame{
    public RectangleFrame() {
        super("Rectangle");
        this.setSize(500, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        JLabel lblWidth = new JLabel("Width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(108, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);
        
        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(165, 5);
        this.add(txtWidth);
        
        JLabel lblHeight = new JLabel("Height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(230, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(285, 5);
        this.add(txtHeight);

        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(180, 50);
        this.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle width= ??? height= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.YELLOW);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    double width = Double.parseDouble(strWidth);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(width, height);
                    lblResult.setText("Rectangle w= " + String.format("%.2f", rectangle.getW())
                            + " h= " + String.format("%.2f", rectangle.getH())
                            + " area= " + String.format("%.2f", rectangle.calArea())
                            + " perimeter= " + String.format("%.2f", rectangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();
                }
            }
        });
        }
        public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);

    }
}
