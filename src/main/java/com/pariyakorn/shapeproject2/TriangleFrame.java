/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class TriangleFrame extends JFrame {
    public TriangleFrame() {
        super("Triangle");
        this.setSize(500, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        JLabel lblBase = new JLabel("Base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(101, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(155, 5);
        this.add(txtBase);

        JLabel lblHeight = new JLabel("Height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(220, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(275, 5);
        this.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(175, 50);
        this.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle base= ??? height= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
         btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
             try {
                    String strBase = txtBase.getText();
                    double base = Double.parseDouble(strBase);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base, height);
                    lblResult.setText("Triangle base= " + String.format("%.2f", triangle.getBase())
                            + " height= " + String.format("%.2f", triangle.getHeight())
                            + " area= " + String.format("%.2f", triangle.calArea())
                            + " perimeter= " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                }
            }
        });
    }
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}

