/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class CircleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Circle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblRadius = new JLabel("radius:",JLabel.TRAILING);
        lblRadius.setSize(50,20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);
        
        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50,20);
        txtRadius.setLocation(60, 5);
        frame.add(txtRadius);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120,5);
        frame.add(btnCalculate);
        
        
        final JLabel lblResult = new JLabel("Circle = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strRadius = txtRadius.getText();
                double radius = Double.parseDouble(strRadius);
                Circle circle = new Circle(radius);
                lblResult.setText("Circle r = " + String.format("%.2f", circle.getRadius())
                        + "area = " + String.format("%.2f", circle.calArea())
                        + "perimeter = " + String.format("%.2f", circle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();

                }
            }
            
        });
        
        frame.setVisible(true);
        
    }
}
