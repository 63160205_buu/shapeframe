/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shapeproject2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author acer
 */
public class SquareFrame extends JFrame {
    public SquareFrame(){
        super("Square");
        this.setSize(500, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        JLabel lblSide = new JLabel("Side:", JLabel.TRAILING);
        lblSide.setSize(35, 20);
        lblSide.setLocation(150, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);
        
        final JTextField txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(190, 5);
        this.add(txtSide);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(260, 5);
        this.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Square Side= ??? area= ??? perimeter= ???", JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.GREEN);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 try{
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lblResult.setText("Square side= " + String.format("%.2f", square.getSide())
                            + " area= " + String.format("%.2f", square.calArea())
                            + " perimeter= " + String.format("%.2f", square.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }   
        });
    }
    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
